﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO.Travels
{
    public class TravelsConsulta
    {
        public string TravelCode { get; set; }
        public Int16 NumberPlaces { get; set; }
        public string Destination { get; set; }
        public string BirthPlace { get; set; }
        public string Price { get; set; }
    }
}
