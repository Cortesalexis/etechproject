﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO.travels_Travellers
{
    public class TTConsulta
    {
        public string Id { get; set; }
        public string IdentificationNumber { get; set; }
        public string TravelCode { get; set; }
        public DateTime Date { get; set; }
    }
}
