﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Travellers
    {
        public string IdentificationNumber { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
    }
}
