﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Travels
    {
        public string TravelCode { get; set; }
        public Int16 NumberPlaces { get; set; }
        public string Destination { get; set; }
        public string BirthPlace { get; set; }
        public string Price { get; set; }
    }
}
