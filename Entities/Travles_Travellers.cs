﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
   public class Travles_Travellers
    {
        public string IdentificationNumber { get; set; }
        public string TravelCode { get; set; }
        public DateTime Date { get; set; }
    }
}
