﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Configuration;
using EtechDAL.Models;

namespace EtechDAL.ContextClass
{
    public class DbContextClass : DbContext
    {
        public DbContextClass()
           : base(ConfigurationManager.ConnectionStrings["Etech"].Name)
        {
        }
        public DbSet<Travels> Travels { get; set; }
        public DbSet<Travellers> Travellers { get; set; }
        public DbSet<travels_Travellers> Traveles_Travellers { get; set; }
    }
}