﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EtechDAL.ContextClass;
using EtechDAL.Models;
using DTO.Travellers;

namespace EtechDAL.Controllers
{
    public class TravellersController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage Create([FromBody]TravellersConsulta travellers)
        {
            try
            {
                if (travellers == null)
                {
                    throw new Exception("It was not created");
                }
                using (DbContextClass db = new DbContextClass())
                {
                    Travellers Travellers = new Travellers
                    {
                        Address = travellers.Address,
                        Name = travellers.Name,
                        IdentificationNumber = travellers.IdentificationNumber,
                        PhoneNumber = travellers.PhoneNumber
                    };
                    db.Travellers.Add(Travellers);
                    db.SaveChanges();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, Travellers);
                    response.Headers.Location = new Uri(Request.RequestUri + Travellers.IdentificationNumber.ToString());
                    return response;

                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetAll(string content)
        {
            try
            {
                using (DbContextClass db = new DbContextClass())
                {
                    List<TravellersRespuesta> LTravellers = new List<TravellersRespuesta>();
                    foreach (Travellers item in db.Travellers.ToList())
                    {
                        LTravellers.Add(new TravellersRespuesta
                        {
                            Address = item.Address,
                            IdentificationNumber = item.IdentificationNumber,
                            Name = item.Name,
                            PhoneNumber = item.PhoneNumber
                        });
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, LTravellers);
                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetById(TravellersConsulta travellers)
        {
            try
            {
                using (DbContextClass db = new DbContextClass())
                {
                    Travellers tra = db.Travellers.FirstOrDefault(x => x.IdentificationNumber == travellers.IdentificationNumber);
                    if (tra == null)
                    {
                        throw new Exception();
                    }
                    TravellersRespuesta traResp = new TravellersRespuesta
                    {
                        Address = tra.Address,
                        IdentificationNumber = tra.IdentificationNumber,
                        Name = tra.Name,
                        PhoneNumber = tra.PhoneNumber
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, traResp);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage Update(TravellersConsulta travellers)
        {
            try
            {
                using (DbContextClass db = new DbContextClass())
                {
                    Travellers Travellers = db.Travellers.FirstOrDefault(x => x.IdentificationNumber == travellers.IdentificationNumber);
                    Travellers.IdentificationNumber = travellers.IdentificationNumber;
                    Travellers.Name = travellers.Name;
                    Travellers.PhoneNumber = travellers.PhoneNumber;
                    Travellers.Address = travellers.Address;
                    db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Updated Suscefully");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage Delete(TravellersConsulta travellers)
        {
            try
            {
                using (DbContextClass db = new DbContextClass())
                {
                    db.Travellers.Remove(db.Travellers.FirstOrDefault(x => x.IdentificationNumber == travellers.IdentificationNumber));
                    db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
