﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EtechDAL.ContextClass;
using DTO.Travels;
using EtechDAL.Models;

namespace EtechDAL.Controllers
{
    public class TravelsController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage Create(TravelsConsulta TravelsConsulta)
        { 
            try
            {
                if (TravelsConsulta == null)
                {
                    throw new Exception("It was not created");
                }
                using (DbContextClass db = new DbContextClass())
                {
                    Travels travels = new Travels
                    {
                        BirthPlace = TravelsConsulta.BirthPlace,
                        Destination = TravelsConsulta.Destination,
                        NumberPlaces = TravelsConsulta.NumberPlaces,
                        Price = TravelsConsulta.Price,
                        TravelCode = Guid.NewGuid().ToString().ToUpper()
                    };

                    db.Travels.Add(travels);
                    db.SaveChanges();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    response.Headers.Location = new Uri(Request.RequestUri + travels.TravelCode.ToString());
                    return response;
                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetAll()
        {
            try
            {
                using (DbContextClass db = new DbContextClass())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, db.Travels.ToList());
                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetById(TravelsConsulta TravelsConsulta)
        {
            try
            {
                using (DbContextClass db = new DbContextClass())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, db.Travels.FirstOrDefault(x => x.TravelCode == TravelsConsulta.TravelCode));
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage Update(TravelsConsulta TravelsConsulta)
        {
            try
            {
                using (DbContextClass db = new DbContextClass())
                {
                    Travels Travel = db.Travels.FirstOrDefault(x => x.TravelCode == TravelsConsulta.TravelCode);
                    Travel.BirthPlace = TravelsConsulta.BirthPlace;
                    Travel.Destination = TravelsConsulta.Destination;
                    Travel.NumberPlaces = TravelsConsulta.NumberPlaces;
                    Travel.Price = TravelsConsulta.Price;
                    db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Updated Suscefully");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage Delete(TravelsConsulta TravelsConsulta)
        {
            try
            {
                using (DbContextClass db = new DbContextClass())
                {
                    db.Travels.Remove(db.Travels.FirstOrDefault(x => x.TravelCode == TravelsConsulta.TravelCode));
                    db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

    }
}
