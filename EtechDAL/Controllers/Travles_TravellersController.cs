﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DTO.travels_Travellers;
using EtechDAL.ContextClass;
using EtechDAL.Models;

namespace EtechDAL.Controllers
{
    public class Travles_TravellersController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage Create([FromBody]TTConsulta TtConsulta)
        {
            try
            {
                if (TtConsulta == null)
                {
                    throw new Exception("It was not created");
                }
                using (DbContextClass db = new DbContextClass())
                {
                    travels_Travellers tt = new travels_Travellers
                    {
                        Id = Guid.NewGuid().ToString().ToUpper(),
                        IdentificationNumber = TtConsulta.IdentificationNumber,
                        TravelCode = TtConsulta.TravelCode,
                        Date = TtConsulta.Date
                    };

                    db.Traveles_Travellers.Add(tt);
                    db.SaveChanges();
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    response.Headers.Location = new Uri(Request.RequestUri + tt.TravelCode.ToString());
                    return response;
                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetAll()
        {
            try
            {
                using (DbContextClass db = new DbContextClass())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, db.Traveles_Travellers.ToList());
                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetById([FromBody]TTConsulta TtConsulta)
        {
            try
            {
                using (DbContextClass db = new DbContextClass())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, db.Traveles_Travellers.FirstOrDefault(x => x.Id == TtConsulta.Id));
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage Update([FromBody]TTConsulta TtConsulta)
        {
            try
            {
                using (DbContextClass db = new DbContextClass())
                {
                    travels_Travellers tt = db.Traveles_Travellers.FirstOrDefault(x => x.Id == TtConsulta.Id);
                    tt.IdentificationNumber = TtConsulta.IdentificationNumber;
                    tt.TravelCode = TtConsulta.TravelCode;
                    tt.Date = TtConsulta.Date;

                    db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Updated Suscefully");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage Delete([FromBody]TTConsulta TtConsulta)
        {
            try
            {
                using (DbContextClass db = new DbContextClass())
                {
                    db.Traveles_Travellers.Remove(db.Traveles_Travellers.FirstOrDefault(x => x.Id == TtConsulta.Id));
                    db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
