﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EtechDAL.Models
{
    public class Travels
    {
        [Key]
        public string TravelCode { get; set; }
        public Int16 NumberPlaces { get; set; }
        public string Destination { get; set; }
        public string BirthPlace { get; set; }
        public string Price { get; set; }
    }
}
