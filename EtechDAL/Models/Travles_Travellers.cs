﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EtechDAL.Models
{
   public class travels_Travellers
    {
        [Key]
        public string Id { get; set; }
        public string IdentificationNumber { get; set; }
        public string TravelCode { get; set; }
        public DateTime Date { get; set; }
    }
}
