﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using EtechDAL.Models;
using DTO.Travellers;
using System.Web.Script.Serialization;

namespace EtechUI.Controllers
{
    public class TravellersController : Controller
    {
        // GET: Travellers
        public ActionResult Index()
        {
            return View();
        }

        // GET: Travellers/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        [HttpPost]
        // GET: Travellers/Create
        public async Task<JsonResult> CreateNewTravell(Travellers travellers)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    Travellers tra = new Travellers
                    {
                        Address = travellers.Address,
                        IdentificationNumber = Guid.NewGuid().ToString().ToUpper(),
                        Name = travellers.Name,
                        PhoneNumber = travellers.PhoneNumber
                    };
                    HttpResponseMessage travel = await client.PostAsJsonAsync("http://localhost:59692/api/Travellers/create",tra);
                    if (travel.IsSuccessStatusCode)
                    {
                        var Traveller = new JavaScriptSerializer().Deserialize<List<Travellers>>(travel.Content.ReadAsStringAsync().Result);
                        return Json(Traveller);
                    }
                    else//new StringContent(new JavaScriptSerializer().Serialize(tra), System.Text.Encoding.UTF8, "aplicattion/json")
                    {
                        return Json(new List<Travellers>());
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new List<Travellers>());
            }



        }

        // POST: Travellers/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Travellers/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Travellers/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Travellers/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Travellers/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
