﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EtechUI.Controllers
{
    public class TravelsController : Controller
    {
        // GET: Travels
        public ActionResult Index()
        {
            return View();
        }

        // GET: Travels/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Travels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Travels/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Travels/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Travels/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Travels/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Travels/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
